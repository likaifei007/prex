package com.prex.auth.authentication.social;

import org.springframework.social.security.SocialAuthenticationFilter;

/**
 * @Classname SocialAuthenticationFilterPostProcessor
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-08 11:41
 * @Version 1.0
 */
public interface SocialAuthenticationFilterPostProcessor {

    void process(SocialAuthenticationFilter socialAuthenticationFilter);
}

