package com.prex.common.social.service;

import com.prex.common.social.entity.GiteeUserInfo;

/**
 * @Classname GiteeService
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) lihaodongmail@163.com
 * @Date 2019-09-16 11:37
 * @Version 1.0
 */
public interface GiteeService {
    /**
     * 获取用户信息
     *
     * @return
     */
    GiteeUserInfo getUserInfo(String code);
}
